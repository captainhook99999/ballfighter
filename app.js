var express = require('express');
//require p2 physics library in the server.
var p2 = require('p2'); 
//get the node-uuid package for creating unique id
var unique = require('node-uuid')

var GROUND = Math.pow(2,0), // 00000000000000000000000000000001 in binary
	OBSTACLE =  Math.pow(2,1), // 00000000000000000000000000000010 in binary
	FOOD = Math.pow(2,2),  // 00000000000000000000000000000100 in binary
	PLAYER = Math.pow(2,3),  // 00000000000000000000000000001000 in binary
	ENEMY = Math.pow(2,4);  // 00000000000000000000000000001000 in binary

var teamData = [
	{	no: 0,		name: "America",		playerCnt: 0,		score: 0},
	{	no: 1,		name: "Europe",			playerCnt: 0,		score: 0},
	{	no: 2,		name: "Asia",			playerCnt: 0,		score: 0},
	{	no: 3,		name: "Africa",			playerCnt: 0,		score: 0},
	{	no: 4,		name: "Ocean",			playerCnt: 0,		score: 0},
	{	no: 5,		name: "Universe",		playerCnt: 0,		score: 0}
];

var PLAYER_SIZE = 40;
var BULLET_SIZE = 16;

var app = express();
var serv = require('http').Server(app);
//get the functions required to move players in the server.
var physicsPlayer = require('./server/physics/playermovement.js');

app.get('/',function(req, res) {
	res.sendFile(__dirname + '/client/index.html');
});
app.use('/client',express.static(__dirname + '/client'));

serv.listen(process.env.PORT || 2000);
console.log("Server started.");

var player_lst = [];

//needed for physics update 
var startTime = (new Date).getTime();
var lastTime;
var timeStep= 1/70; 

//the physics world in the server. This is where all the physics happens. 
//we set gravity to 0 since we are just following mouse pointers.
var world = new p2.World({
  gravity : [0,0]
});
//world.setGlobalStiffness(1e6);
world.setGlobalStiffness(1e18);
world.defaultContactMaterial.restitution = 0.3;

//create a game class to store basic game data
var game_setup = function() {
	//The constant number of bullets in the game
	this.bullet_num = 300;
	this.obstacle_num = 100;
	//bullet object list
	this.bullet_pickup = [];
	this.bullet_bodies = [];

	this.obstacle_bodies = [];
	this.obstacles = [];

	//game size height
	this.canvas_height = 10000;
	//game size width
	this.canvas_width = 10000;

	//set boundary walls
	var xmin = 0, xmax = this.canvas_width,
		ymin = 0, ymax = this.canvas_height;
	// Top plane
	var planeTop = new p2.Body({
		angle: Math.PI,
		position: [0, ymax]
	});

	var planeShape = new p2.Plane();
	planeTop.addShape(planeShape);
	world.addBody(planeTop);

	// Create bottom plane
	var planeBottom = new p2.Body({
		position : [0,ymin],
	});
	planeShape = new p2.Plane();
	planeShape.collisionGroup = GROUND;
	planeShape.collisionMask = -1;
	planeBottom.addShape(planeShape);
	world.addBody(planeBottom);
	// Left plane
	var planeLeft = new p2.Body({
		mass:0,
		angle: -Math.PI/2,
		position: [xmin, 0]
	});
	planeShape = new p2.Plane();
	planeShape.collisionGroup = GROUND;
	planeShape.collisionMask = -1;
	planeLeft.addShape(planeShape);
	world.addBody(planeLeft);

	// Right plane
	var planeRight = new p2.Body({
		mass:0,
		angle: Math.PI/2,
		position: [xmax, 0]
	});
	planeShape = new p2.Plane();
	planeShape.collisionGroup = GROUND;
	planeShape.collisionMask = -1;
	planeRight.addShape(planeShape);
	world.addBody(planeRight);
}

// createa a new game instance
var game_instance = new game_setup();


//a player class in the server
var Player = function (startX, startY, startAngle) {
	this.id;
	this.x = startX;
	this.y = startY;
	this.angle = startAngle;
	this.speed = 500;
	//We need to intilaize with true.
	this.sendData = true;
	this.size = PLAYER_SIZE; 
	this.score = 0;
	this.dead = false;

	this.mybullet = 0; //cjs
}

var bulletpickup = function (max_x, max_y, type, id) {
	this.x = getRndInteger(30, max_x - 30);
	this.y = getRndInteger(30, max_y - 30);
	this.velX = 0;
	this.velY = 0;
	this.type = type; 
	this.id = id; 
	this.powerup; 

	this.myshooter = 0; //cjs
	this.myowner = 0; //cjs
}

//We call physics handler 60fps. The physics is calculated here. 
setInterval(heartbeat, 1000/60);



//Steps the physics world. 
function physics_hanlder() {
	var currentTime = (new Date).getTime();
	timeElapsed = currentTime - startTime;
	var dt = lastTime ? (timeElapsed - lastTime) / 1000 : 0;
    dt = Math.min(1 / 10, dt);
    world.step(timeStep);
}

function heartbeat () {
	//the number of bullet that needs to be generated 
	//in this demo, we keep the bullet always at 100
	var bullet_generatenum = game_instance.bullet_num - game_instance.bullet_pickup.length; 
	
	//add the bullet 
	addbullet(bullet_generatenum);

	//add obstacle
	//console.error( "obstacle count " + game_instance.obstacles.length );
	if(!game_instance.obstacles.length){
		addobstacle();
	}
	//physics stepping. We moved this into heartbeat
	physics_hanlder();

	//remove slow moving bullets
	var j, emitFlag = 0;
	for (j = 0; j < game_instance.bullet_bodies.length; j++) {
		var bullet_body = game_instance.bullet_bodies[j];
/*		console.log(bullet_body.position[0]); //useless
		bullet_body.position[0] += 2;
		bullet_body.position[1] += 2;*/
		emitFlag = 0;
		game_instance.bullet_pickup[j].x = bullet_body.position[0];
		game_instance.bullet_pickup[j].y = bullet_body.position[1];
		if(bullet_body.velocity[0] != 0 || bullet_body.velocity[1] != 0){
			if( Math.abs( bullet_body.velocity[0].toFixed(3) ) <= 10 && 
				Math.abs( bullet_body.velocity[1].toFixed(3) ) <= 10){
					bullet_body.velocity[0] = 0;
					bullet_body.velocity[1] = 0;
					if(game_instance.bullet_pickup[j].myowner == 0 && game_instance.bullet_pickup[j].myshooter == 0 )
						continue;
					console.log("#################################");
					console.log(JSON.stringify(game_instance.bullet_pickup[j]));
					if( game_instance.bullet_pickup[j].myowner != 0 )
						console.error("!!  alredy has owner!");
					if(game_instance.bullet_pickup[j].myowner == 0 && game_instance.bullet_pickup[j].myshooter != 0){
						game_instance.bullet_pickup[j].myowner = 0;
						game_instance.bullet_pickup[j].myshooter = 0;
						emitFlag = 2;
					}
					emitFlag = 1;
				}
//			console.log("Not stopped " + JSONG.stringify(game_instance.bullet_pickup[j]));
		}
		game_instance.bullet_pickup[j].velX = bullet_body.velocity[0];
		game_instance.bullet_pickup[j].velY = bullet_body.velocity[1];

		//conditions
//		console.log(game_instance.bullet_pickup[j].x);
		if(emitFlag){
			io.emit("item_update", game_instance.bullet_pickup[j]);
			console.log(JSON.stringify(game_instance.bullet_pickup[j]));
		}
	}
}

function addbullet(n) {
	
	//return if it is not required to create bullet 
	if (n <= 0) {
		return; 
	}
	
	//create n number of bullets to the game
	for (var i = 0; i < n; i++) {
		//create the unique id using node-uuid
		var unique_id = unique.v4(); 
		var bulletentity = new bulletpickup(
			game_instance.canvas_width,
			game_instance.canvas_height,
			'bullet',
			unique_id
		);
		//add to the world
		var bulletBody = new p2.Body({ //asdf
			mass: 5, //?
			position: [bulletentity.x, bulletentity.y],
			//velocity: [Math.cos(getRndInteger(0,Math.PI)) * 100, Math.sin(getRndInteger(0,Math.PI)) * 100],
			velocity: [0, 0],
			fixedRotation: true
		});
		var circleShape = new p2.Circle({ radius: BULLET_SIZE });
		circleShape.collisionGroup = FOOD;
		circleShape.collisionMask = GROUND | OBSTACLE | FOOD;
		bulletBody.addShape(circleShape);
		bulletBody.damping = 0.8; //damping = go slower afterwards
		
		world.addBody(bulletBody);
		bulletBody.refInfo = bulletentity;

		game_instance.bullet_pickup.push(bulletentity);
		game_instance.bullet_bodies.push(bulletBody);
		console.log( "bullet " + i + " : " + game_instance.bullet_bodies[i].refInfo.x + " " + game_instance.bullet_bodies[i].refInfo.y );
		/*
		game_instance.bullet_bodies[i].refInfo.x = 100;
		console.log( "bullet " + i + " : " + game_instance.bullet_pickup[i].x + " " + game_instance.bullet_pickup[i].y );
		*/
		//set the bullet data back to client
		io.emit("item_update", bulletentity); 
	}
}

function checkOverlap(bodyA, bodyB){
	return bodyA.getAABB().overlaps(bodyB.getAABB());
}

function addobstacle() {
	console.log("Adding obs!");

	for (var i = 0; i < game_instance.obstacle_num; i++) {
		var unique_id = unique.v4(); 
		var obstacle = {};

		var obstacle_body = new p2.Body({
			mass: 0,
			position: [0, 0],
		});

		while(true){
			obstacle = {
				id: unique_id,
				x: getRndInteger(0, game_instance.canvas_width ),
				y: getRndInteger(0, game_instance.canvas_height ),
				width: getRndInteger(30, 600),
				height: getRndInteger(30, 600),
				redColor: getRndInteger(100, 255),
				greenColor: getRndInteger(100, 255),
				blueColor: getRndInteger(100, 255)
			};
			obstacle_body.position[0] = obstacle.x;
			obstacle_body.position[1] = obstacle.y;
			//not easy to modify existing shape so create new one
			var boxShape = new p2.Box({ width: obstacle.width, height: obstacle.height });
			obstacle_body.addShape(boxShape);
			for( var j = 0; j < i; j++){
				if(checkOverlap(obstacle_body, game_instance.obstacle_bodies[j] )){
					break;
				}
			}
			if(j == i){
				break;
			}
			obstacle_body.removeShape(boxShape);
			// obstacle.x = getRndInteger(0, game_instance.canvas_width );
			// obstacle.y = getRndInteger(0, game_instance.canvas_height );
			// obstacle.width = getRndInteger(30, 600);
			// obstacle.height = getRndInteger(30, 600);
		}

		boxShape.collisionGroup = OBSTACLE;
		boxShape.collisionMask = FOOD | PLAYER | OBSTACLE;

		world.addBody(obstacle_body);

		game_instance.obstacles.push(obstacle);
		game_instance.obstacle_bodies.push(obstacle_body);

		/*
		console.log( "bullet " + i + " : " + game_instance.bullet_pickup[i].x + " " + game_instance.bullet_pickup[i].y );
		*/
		//set the bullet data back to client
		console.log(JSON.stringify(obstacle));
	}
}

function onObsRequest(){
/*	console.log("=============== obs Requested"); */
//	game_instance.obstacle_bodies = []; //bug
//	console.log(JSON.stringify(game_instance.obstacles));
	for(var i = 0; i < game_instance.obstacle_num; i++) {
		// this.emit("add_obstacle", {x: game_instance.obstacle_bodies[i].position.x,
		// 							y: game_instance.obstacle_bodies[i].position.y,
		// 						 width: game_instance.obstacle_bodies[i].width,
		// 						 height: game_instance.obstacle_bodies[i].height });
		this.emit("add_obstacle", game_instance.obstacles[i]);
	}
}


// when the player enters their name, trigger this function
function onEntername (data) {
	this.emit('join_game', { username: data.username, teamname: data.teamname,  id: this.id});
}

function getTeamdatafromName(teamname){
	for(var i = 0; i < teamData.length; i++){
		if( teamData[i].name == teamname ){
			return teamData[i];
		}
	}
	return false;
}

// when a new player connects, we make a new instance of the player object,
// and send a new player message to the client. 
function onNewplayer (data) {
	//new player instance
	var newPlayer = new Player(data.x, data.y, data.angle);
	newPlayer.id = this.id;
	newPlayer.username = data.username;
	newPlayer.teamname = data.teamname;
//	var team = getTeamdatafromName(data.teamname);

	console.log("NEW PLAYER: " + JSON.stringify(data));
	
	//create an instance of player body 
	playerBody = new p2.Body ({
		mass: 2,
		position: [0,0],
		fixedRotation: true
	});

			var circleShape = new p2.Circle({ radius: PLAYER_SIZE });
			circleShape.collisionGroup = PLAYER;
			circleShape.collisionMask = OBSTACLE | GROUND | PLAYER;
			playerBody.addShape(circleShape);
			world.addBody(playerBody);

	//add the playerbody into the player object 
	newPlayer.playerBody = playerBody;
	world.addBody(newPlayer.playerBody);
	
	console.log("created new player with id " + this.id);
	newPlayer.id = this.id; 	
	newPlayer.mybullet = 0;
	
	this.emit('create_player', {id: this.id, username:newPlayer.username, teamname:newPlayer.teamname, size: newPlayer.size});
	
	//information to be sent to all clients except sender
	var current_info = {
		id: newPlayer.id, 
		username: data.username,
		teamname: data.teamname,
		x: newPlayer.x,
		y: newPlayer.y,
		angle: newPlayer.angle,
		size: newPlayer.size,
		mybullet: newPlayer.mybullet //cjs
	}; 
	
	//send to the new player about everyone who is already connected. 	
	for (i = 0; i < player_lst.length; i++) {
		existingPlayer = player_lst[i];
		var player_info = {
			id: existingPlayer.id,
			username: existingPlayer.username,
			teamname: existingPlayer.teamname,
			x: existingPlayer.x,
			y: existingPlayer.y, 
			angle: existingPlayer.angle,	
			size: existingPlayer.size,
			mybullet: existingPlayer.mybullet
		};
		console.log("pushing player");
		//send message to the sender-client only
		this.emit("new_enemyPlayer", player_info);
	}
	
	//Tell the client to make bullets that are exisiting
	for (j = 0; j < game_instance.bullet_pickup.length; j++) {
		var bullet_pick = game_instance.bullet_pickup[j];
		bullet_pick.velX = game_instance.bullet_bodies[j].velocity[0];
		bullet_pick.velY = game_instance.bullet_bodies[j].velocity[1];
		this.emit('item_update', bullet_pick); //update client's bullet data
	}
	
	//send message to every connected client except the sender
	this.broadcast.emit('new_enemyPlayer', current_info);
	
	player_lst.push(newPlayer); 

	var team = getTeamdatafromName(newPlayer.teamname);
	team.playerCnt++;
	sortTeamListByScore();
}

function setBulletPosition( id, posx, posy, stopFlag ){
	var ownbulletbody = find_bullet_body(id);
	//console.log( "my bullet:" + JSON.stringify(ownbulletbody.refInfo) );
	ownbulletbody.position[0] = posx;
	ownbulletbody.position[1] = posy;
	ownbulletbody.refInfo.x = posx;
	ownbulletbody.refInfo.y = posy;
	ownbulletbody.refInfo.velX = 0;
	ownbulletbody.refInfo.velY = 0;
	ownbulletbody.velocity[0] = 0;	//stop
	ownbulletbody.velocity[1] = 0;

	if(stopFlag){
		console.error("resetting ownership " + id);
		ownbulletbody.refInfo.myshooter = 0;
		ownbulletbody.refInfo.myowner = 0;
	}

	return ownbulletbody.refInfo;
}

function stopBulletPosition( id ){
	var ownbulletbody = find_bullet_body(id);
	//console.log( "my bullet:" + JSON.stringify(ownbulletbody.refInfo) );
	ownbulletbody.refInfo.x = ownbulletbody.position[0];
	ownbulletbody.refInfo.y = ownbulletbody.position[1];
	ownbulletbody.refInfo.velX = 0;
	ownbulletbody.refInfo.velY = 0;
	ownbulletbody.velocity[0] = 0;	//stop
	ownbulletbody.velocity[1] = 0;

	ownbulletbody.refInfo.myshooter = 0;
	ownbulletbody.refInfo.myowner = 0;
	return ownbulletbody.refInfo;
}


//instead of listening to player positions, we listen to user inputs 
function onInputFired (data) {
	var movePlayer = find_playerid(this.id, this.room); 
	
	
	if (!movePlayer || movePlayer.dead) {
		return;
		console.log('no player'); 
	}

	//when sendData is true, we send the data back to client. 
	if (!movePlayer.sendData) {
		return;
	}
	
	//every 50ms, we send the data. 
	setTimeout(function() {movePlayer.sendData = true}, 50);

	//we set sendData to false when we send the data. 
	movePlayer.sendData = false;
	
	//Make a new pointer with the new inputs from the client. 
	//contains player positions in server
	var serverPointer = {
		x: data.pointer_x,
		y: data.pointer_y,
		worldX: data.pointer_worldx,
		worldY: data.pointer_worldy
	}
	
	//moving the player to the new inputs from the player
	if (physicsPlayer.distanceToPointer(movePlayer, serverPointer) <= PLAYER_SIZE) {
		movePlayer.playerBody.angle = physicsPlayer.movetoPointer(movePlayer, 0, serverPointer, 100);
	} else 
	{
		movePlayer.playerBody.angle = physicsPlayer.movetoPointer(movePlayer, movePlayer.speed, serverPointer);	
	}
	
	movePlayer.x = movePlayer.playerBody.position[0]; 
	movePlayer.y = movePlayer.playerBody.position[1];
	
	//new player position to be sent back to client. 
	var info = {
		x: movePlayer.playerBody.position[0],
		y: movePlayer.playerBody.position[1],
		angle: movePlayer.playerBody.angle
	}

	//send to sender (not to every clients). 
	this.emit('input_recieved', info); //ok go there

	//data to be sent back to everyone except sender 
	var moveplayerData = {
		id: movePlayer.id, 
		x: movePlayer.playerBody.position[0],
		y: movePlayer.playerBody.position[1],
		angle: movePlayer.playerBody.angle,
	}
	
	//send to everyone except sender 
	this.broadcast.emit('enemy_move', moveplayerData); //I ve moved!
	//move also own bullet
//	console.log( "name: " + movePlayer.username + " | " + movePlayer.mybullet );

	if( movePlayer.mybullet != 0 ){
		var ownbullet = setBulletPosition(movePlayer.mybullet,
		 				movePlayer.playerBody.position[0],
		 				movePlayer.playerBody.position[1], 0 ); //big bug

/*		var ownbullet = find_bullet(movePlayer.mybullet);
		console.log("my bullet:" + JSON.stringify(ownbullet));
		ownbullet.x = movePlayer.playerBody.position[0];
		ownbullet.y = movePlayer.playerBody.position[1];
*/
		//console.log("my bullet:" + JSON.stringify(ownbullet));
		this.emit('item_update', ownbullet); //update client's bullet data
		this.broadcast.emit('item_update', ownbullet); //update client's bullet data
	}

	var j;
	for (j = 0; j < game_instance.bullet_bodies.length; j++) {
		var bullet_body = game_instance.bullet_bodies[j];
		if( bullet_body.velocity[0] == 0 && bullet_body.velocity[1] == 0 )	continue;
		console.log("------Moving item: " + JSON.stringify(game_instance.bullet_pickup[j]));
		this.emit('item_update', game_instance.bullet_pickup[j]); //update client's bullet data
//		io.emit("item_update", game_instance.bullet_pickup[j]);
	}
}

function onShootBullet( data ){
	console.log("shoot event!!!!!!!!!!!!!!!!!!");
	var movePlayer = find_playerid(this.id);
	
	if (!movePlayer || movePlayer.dead) {
		console.log('no player'); 
		return;
	}
	//immediately
	if( movePlayer.mybullet == 0 )	return; //cheating

	var ownbullet = find_bullet(movePlayer.mybullet);
	console.log( "shoot bullet:" + JSON.stringify(ownbullet) );

	//set flags
	if( ownbullet.myowner != 0 && ownbullet.myowner != movePlayer.id )
		console.error("!!!!  alredy has owner! " + find_playerid( ownbullet.myowner).username );

	console.log( "shoot bullet:" + JSON.stringify(ownbullet) );

	var ownbulletbody = find_bullet_body(ownbullet.id);
	//velocity: [Math.cos(getRndInteger(0,Math.PI)) * 100, Math.sin(getRndInteger(0,Math.PI)) * 100],
	
	console.log( "player Angle: " + movePlayer.playerBody.angle );
	console.log( "Velocity 0 : " + ownbulletbody.velocity[0] );
	console.log( "Velocity 1 : " + ownbulletbody.velocity[1] );

	ownbulletbody.velocity[0] = Math.cos(movePlayer.playerBody.angle) * 2000;
	ownbulletbody.velocity[1] = Math.sin(movePlayer.playerBody.angle) * 2000;
	ownbulletbody.position[0] = movePlayer.playerBody.position[0]; //update position too
	ownbulletbody.position[1] = movePlayer.playerBody.position[1];
	var TimeStamp = Date.now();
	console.log("Time: " + TimeStamp );
	console.log( "Velocity 0 : " + ownbulletbody.velocity[0] + "\t" + 		  "Velocity 1 : " + ownbulletbody.velocity[1] );
	console.log( "Player   X : " + movePlayer.playerBody.position[0] + "\t" + "Player   Y : " + movePlayer.playerBody.position[1] );
	console.log( "Bullet   X : " + ownbulletbody.position[0] + "\t" + 		  "Bullet   Y : " + ownbulletbody.position[1] );
/*
	this.emit('item_update', ownbullet); //update client's bullet data
	this.broadcast.emit('item_update', ownbullet); //update all
*/
	var self_msg = {
		time: TimeStamp,
		velX: ownbulletbody.velocity[0],
		velY: ownbulletbody.velocity[1],
		x: movePlayer.playerBody.position[0],
		y: movePlayer.playerBody.position[1],
	}
	this.emit("shoot", self_msg);
	var enemy_msg = {
		time: TimeStamp,
		velX : self_msg.velX,
		velY : self_msg.velY,
		x: movePlayer.playerBody.position[0],
		y: movePlayer.playerBody.position[1],
		shooter: this.id,
		bullet: movePlayer.mybullet
	}
	this.broadcast.emit('enemy_shoot', enemy_msg);
	ownbullet.myowner = 0;
	ownbullet.myshooter = movePlayer.id;
	movePlayer.mybullet = 0;
}

function onPlayerCollision(data) {
	var movePlayer = find_playerid(this.id); 
	var enemyPlayer = find_playerid(data.id); 
	
	if (movePlayer.dead || enemyPlayer.dead)
		return
	
	if (!movePlayer || !enemyPlayer)
		return
	
	if (movePlayer.size == enemyPlayer)
		return
	//the main player size is less than the enemy size

	/*
	else if (movePlayer.size < enemyPlayer.size) {
		var gained_size = movePlayer.size / 2;
		enemyPlayer.size += gained_size; 
		this.emit("killed");
		//provide the new size the enemy will become
		this.broadcast.emit('remove_player', {id: this.id});
		this.broadcast.to(data.id).emit("gained", {new_size: enemyPlayer.size}); 
		playerKilled(movePlayer);
	} else {
		var gained_size = enemyPlayer.size / 2;
		movePlayer.size += gained_size;
		this.emit('remove_player', {id: enemyPlayer.id}); 
		this.emit("gained", {new_size: movePlayer.size}); 
		this.broadcast.to(data.id).emit("killed"); 
		//send to everyone except sender.
		this.broadcast.emit('remove_player', {id: enemyPlayer.id});
		playerKilled(enemyPlayer);
	}
	
	console.log("someone ate someone!!!");
	*/
}

function find_bullet (id) {
	for (var i = 0; i < game_instance.bullet_pickup.length; i++) {
		if (game_instance.bullet_pickup[i].id == id) {
			return game_instance.bullet_pickup[i]; 
		}
	}
	return false;
}

function find_bullet_body (id) {
	for (var i = 0; i < game_instance.bullet_pickup.length; i++) {
		if (game_instance.bullet_pickup[i].id == id) {
			return game_instance.bullet_bodies[i]; 
		}
	}
	return false;
}

function sortTeamListByScore() {
	teamData.sort(function(a,b) {
		return b.score - a.score;
	});
	console.log("leader board updated: ");
	// var playerListSorted = [];
	// for (var i = 0; i < player_lst.length; i++) {
	// 	playerListSorted.push({id: player_lst[i].id, username: player_lst[i].username, score: player_lst[i].score});
	// 	console.log(JSON.stringify({id: player_lst[i].id, username: player_lst[i].username, score: player_lst[i].score}));
	// }
	
	io.emit("leader_board", teamData);
}

function onitemPicked (data) {
	var movePlayer = find_playerid(this.id);  //player obj
	if( !movePlayer ){
		console.log(data);
		console.log("onitemPicked: could not find Player");
	}

	var bullet = find_bullet(data.id); //	bullet obj 
	if (!bullet) {
		console.log("onitemPicked: could not find bullet");
		return;
	}

	console.log("collide bullet : " + JSON.stringify(bullet));

	if( bullet.myshooter == 0 && bullet.myowner != 0 ){
		if(bullet.myowner != movePlayer.id){ //other's
			var oldowner = find_playerid(bullet.myowner);
			if(oldowner){ //this guy exists
				console.log( "other guy's bullet:" + bullet.myshooter + " " + bullet.myowner );
				return;
			}
		}
		//this was mine
	}
	
//	if(bullet.myshooter == undefined || bullet.myowner == undefined)	return;

	if( bullet.myshooter != 0 && bullet.myshooter != this.id ){ //enemy's bullet
		var killer = find_playerid( bullet.myshooter );
		if(movePlayer.teamname == killer.teamname){
			console.log( "sameteam");
			return;
		}

		console.log( "shooter " + bullet.myshooter + ", owner " + bullet.myowner+ ", myid " + this.id + " " + movePlayer.id );
		console.log( "bullet.shooter " + bullet.myshooter );
		console.log( "killer " + killer );
		console.log( "killer data: " + killer.id + " " + killer.username + " " + killer.score);
		//killer.score++;
		if( bullet.myowner != 0 )
			console.error("!!!!!  alredy has owner!");
		bullet.myowner = 0;
		bullet.myshooter = 0;
		var team = getTeamdatafromName(killer.teamname);
		team.score++;
		sortTeamListByScore();
//		setBulletPosition(bullet.id, movePlayer.playerBody.position[0], movePlayer.playerBody.position[1], 1 ); //aware of place
		stopBulletPosition(bullet.id); //aware of place
		io.emit("item_update", bullet);
		this.emit("killed");
		this.broadcast.emit('remove_player', {id: this.id});
		//this.broadcast.to(data.id).emit("gained", {new_size: enemyPlayer.size}); 
		playerKilled(movePlayer);
		return;
	}

	if(movePlayer.mybullet != 0 ){
		console.log("already own bullet");
		return;
	}

	//adjust bullet pos
	setBulletPosition(bullet.id, movePlayer.playerBody.position[0], movePlayer.playerBody.position[1], 0 );

	console.log("item picked");
	console.log( movePlayer.username + " picked "  + data.id ); //bullet id
	console.log(JSON.stringify(bullet)); //bullet item
	if( bullet.myowner != 0 )
		console.error("!!!!!!!  alredy has owner!");
	bullet.myowner = movePlayer.id; //my id
	bullet.myshooter = 0;		//very important
	movePlayer.mybullet = bullet.id;
	console.log(JSON.stringify(bullet)); //bullet item

	// io.emit('itemremove', bullet);  //cjs after shoot

	var bulletbody = find_bullet_body(data.id);
	bulletbody.sensor = true;

	this.emit("gained", {owner: movePlayer.id, bullet: bullet.id, x: bullet.x, y:bullet.y });  //cjs
	//this.emit('item_picked'); //hey you ate bullet
}

function playerKilled (player) {
	//find the player and remove it.
	var removePlayer = find_playerid(player.id); 
		
	if (removePlayer) {
		player_lst.splice(player_lst.indexOf(removePlayer), 1);
	}

	if(removePlayer.mybullet != 0){ //release owned bullet
		var bullet = stopBulletPosition(removePlayer.mybullet);
		io.emit("item_update", bullet);
		// var bullet = find_bullet(removePlayer.mybullet);
		// if(bullet){
		// 	bullet.myshooter = 0;
		// 	bullet.myowner = 0;
		// }
	}
	player.dead = true; 
	var team = getTeamdatafromName(removePlayer.teamname);
	team.playerCnt--;

	sortTeamListByScore();
}

function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}

//call when a client disconnects and tell the clients except sender to remove the disconnected player
function onClientdisconnect() {
	console.log('disconnect'); 

	var removePlayer = find_playerid(this.id); 
		
	if (removePlayer) {
		player_lst.splice(player_lst.indexOf(removePlayer), 1);
	}
	
	console.log("removing player " + this.id);
	var team = getTeamdatafromName(removePlayer.teamname);
	team.playerCnt--;

	sortTeamListByScore();
	//send message to every connected client except the sender
	this.broadcast.emit('remove_player', {id: this.id});
}

// find player by the the unique socket id 
function find_playerid(id) {

	for (var i = 0; i < player_lst.length; i++) {

		if (player_lst[i].id == id) {
			return player_lst[i]; 
		}
	}
	
	return false; 
}

 // io connection 
var io = require('socket.io')(serv,{});

io.sockets.on('connection', function(socket){
	console.log("socket connected"); 
	//when the player enters their name
	socket.on('enter_name', onEntername); 
	
	//whent he player logs in
	socket.on('logged_in', function(data){
		this.emit('enter_game', {username: data.username, teamname:data.teamname}); 
	}); 
	
	// listen for disconnection; 
	socket.on('disconnect', onClientdisconnect); 
	
	// listen for new player
	socket.on("new_player", onNewplayer);
	
	//shoot
	socket.on("shoot_bullet", onShootBullet);
	
	//listen for new player inputs. 
	socket.on("input_fired", onInputFired);
	
//	socket.on("player_collision", onPlayerCollision);
	
	//listen if player got items 
	socket.on('item_picked', onitemPicked);

	socket.on("gimme_obstacle", onObsRequest);
});