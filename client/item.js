//the bullet list
var bullet_pickup = [];
var com_cnt = 0;

// search through bullet list to find the bullet object
function finditembyid (id) {
	
	for (var i = 0; i < bullet_pickup.length; i++) {

		if (bullet_pickup[i].id == id) {
			return bullet_pickup[i]; 
		}
	}
	
	return false; 
}

// function called when new bullet is added in the server.
function onitemUpdate (data) {
//	console.log("%%%%%%%%%%%%%%%%%%%%%%%");
	var bullet;
	bullet = finditembyid(data.id);
	
	if( bullet ){ //modify
//		return;
		bullet.posx = data.x;
		bullet.posy = data.y;
/*		bullet.item.body.x = data.x;  //moving here?
		bullet.item.body.y = data.y;*/
		{
			bullet.item1.body.x = data.x;
			bullet.item1.body.y = data.y;
			bullet.item1.body.velocity.x = data.velX;
			bullet.item1.body.velocity.y = data.velY;

			if(data.velX == 0 && data.velY == 0 && data.myowner == 0 && data.myshooter == 0 /*gameProperties.in_game == false*/){
				bullet.item.body.x = data.x;
				bullet.item.body.y = data.y;
				bullet.item.body.velocity.x = 0;
				bullet.item.body.velocity.y = 0;
			}else{
				var newPointer = {
					x: data.x,
					y: data.y, 
					worldX: data.x,
					worldY: data.y, 
				};
				var distance = distanceToPointer(bullet.item, newPointer);
				var speed = distance/0.05;
				movetoPointer(bullet.item, speed, newPointer);
			}

			// bullet.item.body.x = data.x;
			// bullet.item.body.y = data.y;
			// bullet.item.body.velocity.x = data.velX;
			// bullet.item.body.velocity.y = data.velY;

			// console.log( "(" + bullet.item.body.x + ", " + bullet.item.body.y + ") " +
			// "(" + data.x + ", " + data.y + ") " +
			//  "distance : " + distance );
		}
		bullet.item.myowner = data.myowner;
		bullet.item.myshooter = data.myshooter;
		//console.log("updated " + data.myowner + "  " + data.myshooter);
		//bullet.item.body.velocity.x = 150;
		//console.log( "communication count : " +( com_cnt++) + JSON.stringify(data) );
	}
	else{
		bullet_pickup.push(new bullet_object(data.id, data.type, data.x, data.y, 0, 0)); 
	}
}

// function called when bullet needs to be removed in the client. 
function onitemremove (data) {
	
	var removeItem; 
	removeItem = finditembyid(data.id);
	bullet_pickup.splice(bullet_pickup.indexOf(removeItem), 1); 
	
	//destroy the phaser object 
	removeItem.item.destroy(true,false);
	
}

// the bullet class
var bullet_object = function (id, type, startx, starty, value, shooter, owner) {
	// unique id for the bullet.
	//generated in the server with node-uuid
	this.id = id; 
	
	//positinon of the bullet
	this.posx = startx;  
	this.posy = starty; 
	this.powerup = value;
	
	//create a circulr phaser object for bullet
	this.item = game.add.graphics(this.posx, this.posy);
	this.item.beginFill(Phaser.Color.getRandomColor());
	//this.item.lineStyle(2, 0x00FF00, 1);
	this.item.drawCircle(0, 0, BULLET_SIZE * 2);

	this.item.type = 'bullet_body';
	this.item.id = id;
	
	game.physics.p2.enableBody(this.item, false);
	//this.item.body.clearShapes();
	this.item.body_size = BULLET_SIZE; 
	this.item.body.addCircle(BULLET_SIZE, 0, 0);
	//this.item.body.data.gravityScale = 0;
	//this.item.body.data.shapes[0].sensor = true;
	this.item.body.setCollisionGroup(bulletCollisionGroup);
	this.item.body.collides([/*bulletCollisionGroup,*/ obstacleCollisionGroup]);

	this.item.body.addCircle(BULLET_SIZE, 0, 0);  //to collide with player
	this.item.body.data.shapes[1].sensor = true;

	//physical properties
	this.item.body.mass = 5;
	this.item.body.damping = 0.8;
	this.item.body.fixedRotation = true;

	this.item.myshooter = 0; //cjs
	this.item.myowner = 0; //cjs

	this.item1 = game.add.graphics(this.posx, this.posy);
	// this.item1.beginFill(0x00FF00);
	// this.item1.drawCircle(0, 0, 20);
	// this.item1.endFill();
	game.physics.p2.enableBody(this.item1, false); ///?????
	
	//this.item1.body.clearShapes();
	this.item1.body_size = BULLET_SIZE; 
	this.item1.body.addCircle(BULLET_SIZE, 0, 0);
	this.item1.body.setCollisionGroup(serverBulletCollisionGroup);
	this.item1.body.mass = 5;
	this.item1.body.damping = 0.8;
	this.item1.body.fixedRotation = true;
//	this.item1.body.collides([enemyCollisionGroup]);
	//this.item1.body.data.shapes[0].sensor = true;
	frontLayer.add(this.item1);
	bulletLayer.add(this.item);
}

