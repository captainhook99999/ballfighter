canvas_width = window.innerWidth * window.devicePixelRatio;
canvas_height = window.innerHeight * window.devicePixelRatio;

game = new Phaser.Game(canvas_width,canvas_height, Phaser.CANVAS, 'gameDiv');

var teamData = {
	"America":
		{redColor: 255,		greenColor: 50,		blueColor: 50},
	"Europe":
		{redColor: 50,		greenColor: 255,	blueColor: 50},
	"Asia":
		{redColor: 50,		greenColor: 50,		blueColor: 255},
	"Africa":
		{redColor: 50,		greenColor: 255,	blueColor: 255},
	"Ocean":
		{redColor: 255,		greenColor: 50,		blueColor: 255},
	"Universe":
		{redColor: 255,		greenColor: 255,	blueColor: 50},
};

var PLAYER_SIZE = 40;
var BULLET_SIZE = 16;

//the enemy player list 
var enemies = [];
var obstacles = [];

var bkLayer;
var playerLayer ;
var bulletLayer ;
var obstacleLayer ;
var frontLayer;

var playerCollisionGroup;
var enemyCollisionGroup;
var obstacleCollisionGroup;
var bulletCollisionGroup;
var serverBulletCollisionGroup;

var gameProperties = {
	gameWidth: 10000,
	gameHeight: 10000,
	obstacle_num: 100,
	game_elemnt: "gameDiv",
	in_game: false,
};

var main = function(game){
};

function onsocketConnected (data) {
	console.log("connected to server"); 
	gameProperties.in_game = true;
	var username = data.username;
	// send the server our initial position and tell it we are connected
	socket.emit('new_player', {username: data.username, teamname: data.teamname, x: 0, y: 0, angle: 0});
}

// When the server notifies us of client disconnection, we find the disconnected
// enemy and remove from our game
function onRemovePlayer (data) {
	var removePlayer = findplayerbyid(data.id);
	// Player not found
	if (!removePlayer) {
		console.log('Player not found: ', data.id)
		return;
	}
	
	removePlayer.player.destroy();
	enemies.splice(enemies.indexOf(removePlayer), 1);
}

function createPlayer (data) {
//	alert(bkLayer);
	player = game.add.graphics(0, 0);
	player.radius = PLAYER_SIZE;
	player.username = data.username;
	player.teamname = data.teamname;
	player.id = data.id;

	// set a fill and line style
	player.beginFill(Phaser.Color.getColor(teamData[data.teamname].redColor, 
											teamData[data.teamname].greenColor,
											teamData[data.teamname].blueColor ));
	//player.lineStyle(2, 0xffd900, 1);
	player.drawCircle(0, 0, PLAYER_SIZE * 2);
	player.endFill();
	player.anchor.setTo(0.5,0.5);
	player.body_size = PLAYER_SIZE; 
	//set the initial size;
	player.initial_size = PLAYER_SIZE;
	player.type = "player_body"; 
	player.mybullet = 0; //cjs
	player.shootable = 0;

	// draw a shape
	game.physics.p2.enableBody(player, false); //false
	//player.body.clearShapes();
	player.body.setCircle(PLAYER_SIZE); 
	//player.body.fixedRotation = true;
	//player.body.setCollisionGroup(playerCollisionGroup);
	//player.body.collides(bulletCollisionGroup, player_coll);
	// player.body.collides( obstacleCollisionGroup, function(){
	// 	console.log("collides with obstacle");
	// });
	player.body.data.shapes[0].sensor = true;
	//enable colellision and when it makes a contact with another body, call player_coll
	player.body.onBeginContact.add(player_coll, this); 

	//player follow text
	var style = {font: "30px Consolas", fill: "black", wordWrap: true, boundsAlignH: "center",
				align: "center", wordWrapWidth: PLAYER_SIZE};

	player.playertext = game.add.text(0, -70, data.username , style);
	player.playertext.anchor.set(0.5);
	//player.playertext.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
	player.playertext.stroke = '#FFFFFF';
    player.playertext.strokeThickness = 6;
	//frontLayer.add(player.playertext)
	player.addChild(player.playertext);
	
	playerLayer.add(player);
	//camera follow
	game.camera.follow(player, Phaser.Camera.FOLLOW_LOCKON, 0.5, 0.5);
}

//get random intenger
function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}

// this is the enemy class. 
var remote_player = function (id, username, teamname, startx, starty, startSize, start_angle, mybullet) {
	this.x = startx;
	this.y = starty;
	this.username = username; //cjs
	this.teamname = teamname;
//	this.player.username = username;//cjs

	//this is the unique socket id. We use it as a unique name for enemy
	this.id = id;
	this.angle = start_angle;
	
	this.player = game.add.graphics(this.x , this.y);
	//intialize the size with the server value
	this.player.radius = PLAYER_SIZE;

	// set a fill and line style
	this.player.beginFill(0xffd900);
	this.player.beginFill(Phaser.Color.getColor(teamData[teamname].redColor, 
		teamData[teamname].greenColor,
		teamData[teamname].blueColor ));
	//this.player.lineStyle(2, 0xffd900, 1);
	this.player.drawCircle(0, 0, PLAYER_SIZE * 2);
	this.player.endFill();
	this.player.anchor.setTo(0.5,0.5);
	//we set the initial size;
	this.initial_size = PLAYER_SIZE;
	this.player.body_size = PLAYER_SIZE; 
	this.player.type = "player_body";
	this.player.id = this.id;

	// draw a shape
	game.physics.p2.enableBody(this.player, false);
	this.player.body.clearShapes();
	this.player.body.addCircle(PLAYER_SIZE, 0 , 0); 
	this.player.body.data.shapes[0].sensor = true;

	//player follow text
	var style = {font: "24px Consolas", fill: "black", wordWrap: true, boundsAlignH: "center",
				align: "center", wordWrapWidth: PLAYER_SIZE};
	this.player.playertext = game.add.text(0, -70, username , style);
	this.player.playertext.anchor.set(0.5);
	//this.player.playertext.setShadow(2, 2, 'rgba(0,0,0,0.5)', 2);
	this.player.playertext.stroke = '#FFFFFF';
    this.player.playertext.strokeThickness = 4;

	this.player.addChild(this.player.playertext);

	playerLayer.add(this.player);
	this.player.mybullet = mybullet;//cjs
}

//Server will tell us when a new enemy player connects to the server.
//We create a new enemy in our game.
function onNewPlayer (data) {
	//enemy object 
	console.log(data);
	var new_enemy = new remote_player(data.id, data.username, data.teamname, data.x, data.y, data.size, data.angle, data.mybullet); 
	enemies.push(new_enemy);
}

//Server tells us there is a new enemy movement. We find the moved enemy
//and sync the enemy movement with the server
function onEnemyMove (data) {
	var movePlayer = findplayerbyid (data.id); 
	
	if (!movePlayer) {
		return;
	}
	
	var newPointer = {
		x: data.x,
		y: data.y, 
		worldX: data.x,
		worldY: data.y, 
	}
	
	var distance = distanceToPointer(movePlayer.player, newPointer);
	speed = distance/0.05;
	
	movePlayer.rotation = movetoPointer(movePlayer.player, speed, newPointer);

	if(movePlayer.mybullet != 0){
		var ownbullet = finditembyid(movePlayer.mybullet);
		if(!ownbullet) return;
		distance = distanceToPointer(ownbullet.item, newPointer);
		speed = distance/0.05;
		movetoPointer(ownbullet.item, speed, newPointer);
	}
}

//we're receiving the calculated position from the server and changing the player position
function onInputRecieved (data) {
	
	//we're forming a new pointer with the new position
	var newPointer = {
		x: data.x,
		y: data.y, 
		worldX: data.x,
		worldY: data.y, 
	}
	
	var distance = distanceToPointer(player, newPointer);
	//we're receiving player position every 50ms. We're interpolating 
	//between the current position and the new position so that player
	//does jerk. 
	speed = distance/0.05;
	
	player.rotation = movetoPointer(player, speed, newPointer);

	if(player.mybullet != 0){
		var ownbullet = finditembyid(player.mybullet);
		if(!ownbullet) return;
		distance = distanceToPointer(ownbullet.item, newPointer);
		speed = distance/0.05;
		movetoPointer(ownbullet.item, speed, newPointer);
	}
}

function onGained (data) { //from server  self or enemy
	console.log( "========================================================");
	console.log( "onGained : " + JSON.stringify(data) );

	var bullet = finditembyid(data.bullet);
	player.mybullet = data.bullet; //cjs
	player.shootable = 1;
	bullet.item.myowner = data.owner;
	bullet.item.myshooter = 0;

	bullet.posx = data.x;
	bullet.posy = data.y;

	var newPointer = {
		x: data.x,
		y: data.y,
		worldX: data.x,
		worldY: data.y,
	};
	var distance = distanceToPointer(bullet.item, newPointer);
	var speed = distance / 0.05;
	movetoPointer(bullet.item, speed, newPointer);

	//updating shooter's position may be coinsidered.

		// console.log( "(" + bullet.item.body.x + ", " + bullet.item.body.y + ") " +
		// "(" + data.x + ", " + data.y + ") " +
		//  "distance : " + distance );

	//create new body
/*	player.body.clearShapes();
	player.body.addCircle(player.body_size, 0 , 0); 
	player.body.data.shapes[0].sensor = true;*/
	/************************ */
}


function onEnemyGained (data) { //enemy from server
	console.log( "========================================================");
	console.log( "onGained : " + JSON.stringify(data) );

	var owner = findplayerbyid(data.owner);
	var bullet = finditembyid(data.bullet);
	owner.mybullet = data.bullet; //cjs
	bullet.item.myowner = data.owner;
	bullet.item.myshooter = 0;

	bullet.posx = data.x;
	bullet.posy = data.y;

	var newPointer = {
		x: data.x,
		y: data.y,
		worldX: data.x,
		worldY: data.y,
	};
	var distance = distanceToPointer(bullet.item, newPointer);
	var speed = distance / 0.05;
	movetoPointer(bullet.item, speed, newPointer);

	//updating shooter's position may be cosidered.

		// console.log( "(" + bullet.item.body.x + ", " + bullet.item.body.y + ") " +
		// "(" + data.x + ", " + data.y + ") " +
		//  "distance : " + distance );

	//create new body
/*	player.body.clearShapes();
	player.body.addCircle(player.body_size, 0 , 0); 
	player.body.data.shapes[0].sensor = true;*/
	/************************ */
}

function onShoot (data) { //from server
	console.log( "--------------------------------");
	console.log( "onShot : " + JSON.stringify(data) );
//	alert("onShoot");

	var ownbullet = finditembyid(player.mybullet);

	if(!ownbullet){
		console.log("Noooooooooooooooooo bullet!");
		return;
	}
	ownbullet.item.myowner = 0;
	ownbullet.item.myshooter = this.id;
	player.mybullet = 0;

	//velocity: [Math.cos(getRndInteger(0,Math.PI)) * 100, Math.sin(getRndInteger(0,Math.PI)) * 100],
	
	//console.log( "player Angle: " + ownbullet.item.angle );
	var timeStamp = Date.now();
	console.log( "TimeElapse: " + (timeStamp - data.time) );
	//console.log( "Time: " + timeStamp );
	console.log( "Velocity 0 : " + ownbullet.item.body.velocity.x );
	console.log( "Velocity 1 : " + ownbullet.item.body.velocity.y );
	
	ownbullet.item.body.velocity.x = data.velX;
	ownbullet.item.body.velocity.y = data.velY;
	ownbullet.item.position.x = data.x;
	ownbullet.item.position.y = data.y;
	console.log( "Velocity 0 : " + ownbullet.item.body.velocity.x );
	console.log( "Velocity 1 : " + ownbullet.item.body.velocity.y );

/*	this.emit('item_update', ownbullet); //update client's bullet data
	this.broadcast.emit('item_update', ownbullet); //update all
*/

}

function onEnemyShoot (data) { //from server
	console.log( "--------------------------------");
	console.log( "onShot : " + JSON.stringify(data) );
//	alert("onEnemyShoot");

	var ownbullet = finditembyid(data.bullet);
	var shooter = findplayerbyid(data.shooter);
	
	ownbullet.myowner = 0;
	ownbullet.myshooter = data.shooter;
	shooter.mybullet = 0;

	var timeStamp = Date.now();
	// console.log( "TimeElapse: " + (timeStamp - data.time) );
	// //console.log( "Time: " + timeStamp );
	// console.log( "Velocity 0 : " + ownbullet.item.body.velocity.x );
	// console.log( "Velocity 1 : " + ownbullet.item.body.velocity.y );

	if(!ownbullet){
		console.log("no bullet");
	}
	if(!shooter){
		console.log("no shooter");
	}
	
	ownbullet.item.body.velocity.x = data.velX;
	ownbullet.item.body.velocity.y = data.velY;
	ownbullet.item.position.x = data.x;
	ownbullet.item.position.y = data.y;
	// console.log( "Velocity 0 : " + ownbullet.item.body.velocity.x );
	// console.log( "Velocity 1 : " + ownbullet.item.body.velocity.y );
}


function onKilled(data) {
	player.destroy();
	gameProperties.in_game = false;
	setTimeout(()=>{
		location.reload();
	}, 3000);
}


//This is where we use the socket id. 
//Search through enemies list to find the right enemy of the id.
function findplayerbyid (id) {
	if(id == player.id){
		return player;
	}

	for (var i = 0; i < enemies.length; i++) {
		if (enemies[i].id == id) {
			return enemies[i]; 
		}
	}
}

//Here create obstacle box 
function onAddObstacle(data){
	console.log("adding obs" + JSON.stringify(data));
	var obs = game.add.graphics(data.x , data.y);
	obs.anchor.setTo(0.5);

	// set a fill and line style
//	obs.beginFill(0xFF3300);
	obs.beginFill(Phaser.Color.getColor(data.redColor, data.greenColor, data.blueColor));
//	obs.lineStyle(10, 0xffd900, 1);
	//obs.drawRect(0, 0, data.width, data.height);
	obs.drawRect(-data.width / 2, -data.height/2, data.width, data.height);
	obs.endFill();
//  draw a shape
	game.physics.p2.enableBody(obs, false); //with debug?
	
	//obs.body.setRectangle(data.width, data.height);
	//this.item.body.data.gravityScale = 0;
	//this.item.body.data.shapes[0].sensor = true;
	obs.body.collideWorldBounds=false;

	obs.body.setCollisionGroup(obstacleCollisionGroup);
	obs.body.collides([/*playerCollisionGroup,*/ bulletCollisionGroup]);
	obs.body.allowGravity = false;
	obs.body.kinematic = true; //in arcade it's .immovable
    obs.body.moves = false;
    obs.body.gravity.x = 0;
    obs.body.gravity.y = 0;
    obs.body.velocity.x = 0;
    obs.body.velocity.y = 0;
    // obs.body.enable = false;
	obstacleLayer.add(obs);
	obstacles.push(obs);

}

//create leader board in here.
function createLeaderBoard() {
	var leaderBox = game.add.graphics(game.width * 0.7, game.height * 0.05);
	leaderBox.fixedToCamera = true;
	// draw a rectangle
	leaderBox.beginFill(0xD3D3D3, 0.5);
    //leaderBox.lineStyle(2, 0x202226, 1);
    leaderBox.drawRect(0, 0, 300, 400);
	
	var style = { font: "13px Press Start 2P", fill: "black", align: "left", fontSize: '22px'};
	
	leader_text = game.add.text(10, 10, "", style);
	leader_text.anchor.set(0);

	leaderBox.addChild(leader_text);
}

//leader board
function lbupdate (data) {
	//this is the final board string.
	var board_string = ""; 
	var maxlen = 15;
	var maxTeamDisplay = 10;
	var mainTeamShown = false;
	
	for (var i = 0;  i < data.length; i++) {
		//if the mainteam is shown along the iteration, set it to true
	
		if (mainTeamShown && i >= maxTeamDisplay) {
			break;
		}
		
		//if the team's rank is very low, we display maxTeamDisplay - 1 names in the leaderboard
		// and then add three dots at the end, and show team's rank.
		if (!mainTeamShown && i >= maxTeamDisplay - 1 && socket.id == data[i].id) {
			board_string = board_string.concat(".\n");
			board_string = board_string.concat(".\n");
			board_string = board_string.concat(".\n");
			mainTeamShown = true;
		}
		
		//here we are checking if team id is greater than 10 characters, if it is 
		//it is too long, so we're going to trim it.
		if (data[i].name.length >= 15) {
			var name = data[i].name;
			var temp = ""; 
			for (var j = 0; j < maxlen; j++) {
				temp += name[j];
			}
			
			temp += "...";
			name = temp;
		
			board_string = board_string.concat(i + 1,": ");
			board_string = board_string.concat(name," ",(data[i].score).toString() + "\n");
		
		} else {
			board_string = board_string.concat(i + 1,": ");
			board_string = board_string.concat(data[i].name," ", data[i].playerCnt," members, ",(data[i].score).toString() + "kills\n");
		}
	}
	console.log(board_string);
	leader_text.setText(board_string); 
}

main.prototype = {
	init: function(username, teamname) {
		// when the socket connects, call the onsocketconnected and send its information to the server 
		socket.emit('logged_in', {username: username, teamname: teamname}); 
		
		// when the player enters the game 
		socket.on('enter_game', onsocketConnected); 
	},
	
	preload: function() {
		game.stage.disableVisibilityChange = true;
		game.scale.scaleMode = Phaser.ScaleManager.RESIZE;
		game.world.setBounds(0, 0, gameProperties.gameWidth, gameProperties.gameHeight, false, false, false, false);
		game.physics.startSystem(Phaser.Physics.P2JS);
		game.physics.p2.setBoundsToWorld(true, true, true, true, true);
		game.physics.p2.restitution = 0.3;
		game.physics.p2.applyGravity = false; 
//		game.physics.p2.enableBody(game.physics.p2.walls, true); 
		game.physics.p2.updateBoundsCollisionGroup();
		// physics start system
		game.physics.p2.setImpactEvents(true);

		playerCollisionGroup = game.physics.p2.createCollisionGroup();
		enemyCollisionGroup = game.physics.p2.createCollisionGroup();
		obstacleCollisionGroup = game.physics.p2.createCollisionGroup();
		bulletCollisionGroup = game.physics.p2.createCollisionGroup();
		serverBulletCollisionGroup = game.physics.p2.createCollisionGroup();

		bkLayer = game.add.group();
		playerLayer = game.add.group();
			// playerLayer.enableBody = true;
			// playerLayer.physicsBodyType = Phaser.Physics.P2JS;
		bulletLayer = game.add.group();
			// bulletLayer.enableBody = true;
			// bulletLayer.physicsBodyType = Phaser.Physics.P2JS;
		obstacleLayer = game.add.group();
			// obstacleLayer.enableBody = true;
			// obstacleLayer.physicsBodyType = Phaser.Physics.P2JS;
		frontLayer = game.add.group();
    },
	
	create: function () {
		game.stage.backgroundColor = 0;
/*		backgroundSprite = game.add.tileSprite(0,0,game.width,game.height,'jpgbg')

		game.add.sprite(200, 200, 'bkgrid');

		a = game.add.graphics(0, 0);
		a.beginFill(0xffd900);
		a.lineStyle(2, 0xffd900, 1);
		a.drawCircle(0, 0, 200 * 2);

		alert("gfjhdsfgjhsed");
*/
		var grid = game.add.graphics(0 , 0);
		grid.lineStyle(1, 0x0f0f0f, 1);

		// set a fill and line style
		var x, y;
		for( x = 0; x < gameProperties.gameWidth; x += 100 ){
			grid.moveTo(x, 0);
			grid.lineTo(x, gameProperties.gameHeight );
		}
		for( y = 0; y < gameProperties.gameHeight; y += 100 ){
			grid.moveTo(0, y);
			grid.lineTo(gameProperties.gameWidth, y );
		}
		// grid.drawRect(0, 0, game.width, game.height);
		grid.endFill();
		bkLayer.add(grid);
		
		console.log("client started"); 
		
		socket.on('add_obstacle', onAddObstacle);
		//listen for main player creation
		socket.on("create_player", createPlayer);
		//listen to new enemy connections
		socket.on("new_enemyPlayer", onNewPlayer);
		//listen to enemy movement 
		socket.on("enemy_move", onEnemyMove);
		//when received remove_player, remove the player passed; 
		socket.on('remove_player', onRemovePlayer); 
		//when the player receives the new input
		socket.on('input_recieved', onInputRecieved);
		//when the player gets killed
		socket.on('killed', onKilled);
		//when the player is allowed to shoot
		socket.on('shoot', onShoot);
		//when any player shoot
		socket.on('enemy_shoot', onEnemyShoot);
		//when any player gains a bullet
		socket.on('gained', onGained);
		// check for item removal
		socket.on('itemremove', onitemremove); 
		// check for item update
		socket.on('item_update', onitemUpdate); 
		// check for leaderboard
		socket.on ('leader_board', lbupdate); 
		
		createLeaderBoard();
	},
	
	update: function () {
		// emit the player input
		//move the player when the player is made 
		if (gameProperties.in_game) {

			//game.physics.p2.collide(player, bulletLayer);
		
			//we're making a new mouse pointer and sending this input to 
			//the server.
			var pointer = game.input.mousePointer;
					
			//Send a new position data to the server 
			socket.emit('input_fired', {
				pointer_x: pointer.x, 
				pointer_y: pointer.y, 
				pointer_worldx: pointer.worldX, 
				pointer_worldy: pointer.worldY, 
			});

			if(game.input.activePointer.leftButton.isDown && player.shootable && player.mybullet != 0 ){ //shoot
				console.log("SHOOT! " + player.username);
				socket.emit("shoot_bullet", {
					pointer_x: pointer.x,
					pointer_y: pointer.y,
					pointer_worldx: pointer.worldX,
					pointer_worldy: pointer.worldY,
				});
//				player.mybullet = 0;

				//socket.emit("gimme_obstacle", {id: this.id});
//			 	alert("shoot");
			}
			
			
			if(obstacles.length == 0){
				socket.emit("gimme_obstacle", {id: this.id});
			}
		}
	}
}

var gameBootstrapper = {
    init: function(gameContainerElementId){
		game.state.add('main', main);
		game.state.add('login', login);
		game.state.start('login'); 
    }
};;

gameBootstrapper.init("gameDiv");